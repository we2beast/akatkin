package main

import (
	"fmt"
	"github.com/tidwall/gjson"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	xj "github.com/basgys/goxml2json"
)

type Branch struct {
	Exist string
}

type Response struct {
	SDMX []string `json:"sdmx"`
}

type ViewData struct {
	Identifier map[string]string
	URL        string
	Error      string
}

var (
	titleSearch = ""
	codeSearch  = ""
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost {
			url := "https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&file=table_of_contents.xml"

			resp, err := http.Head(url)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			if resp.StatusCode != http.StatusOK {
				fmt.Println(resp.Status)
				os.Exit(1)
			}

			// Получаем размер файла
			size, _ := strconv.Atoi(resp.Header.Get("Content-Length"))
			downloadSize := int64(size)

			file, err := os.Open("table_of_contents.xml")
			if err != nil {
				// handle the error here
				return
			}
			defer file.Close()

			// get the file size
			stat, err := file.Stat()
			if err != nil {
				return
			}

			// Сравниваем размеры файлов
			if stat.Size() != downloadSize {
				// Если размеры разные, то скачиваем файл
				err := DownloadFile("table_of_contents.xml", url)
				if err != nil {
					panic(err)
				}
			}

			// Если размеры одинаковы или файл новый скачали, то считываем содержимое файла и работаем с ним
			dataXML, err := ioutil.ReadFile("table_of_contents.xml")
			if err != nil {
				fmt.Println("File reading error", err)
				return
			}

			jsonText, err := xj.Convert(strings.NewReader(string(dataXML)))
			if err != nil {
				panic("That's embarrassing...")
			}

			var branch Branch
			titleSearch = r.FormValue("title")
			codeSearch = r.FormValue("code")
			var res Response

			value := gjson.Get(jsonText.String(), "tree.branch.#")
			for i := 0; i < int(value.Num); i++ {
				searchItem(jsonText.String(), "tree.branch."+strconv.Itoa(i)+".children.", &branch, &res)
			}

			data := Response{
				SDMX: res.SDMX,
			}

			tmpl, _ := template.ParseFiles("task.html")
			tmpl.Execute(w, data)
			return
		} else {
			tmpl, _ := template.ParseFiles("task.html")
			tmpl.Execute(w, nil)
			return
		}
	})

	http.HandleFunc("/task2", func(w http.ResponseWriter, r *http.Request) {
		text, err := GetXML("https://fedstat.ru/opendata/list.xml")
		if err != nil {
			log.Printf("Failed to get XML: %v", err)
		}

		jsonText, err := xj.Convert(strings.NewReader(text))
		if err != nil {
			panic("That's embarrassing...")
		}

		test := make(map[string]string)
		result := gjson.Get(jsonText.String(), `list.meta.item.#.identifier`).Array()
		result2 := gjson.Get(jsonText.String(), `list.meta.item.#.title`).Array()

		for i := 0; i < len(result2); i++ {
			test[result2[i].String()] = result[i].String()
		}

		identifier := r.FormValue("identifier")
		title := r.FormValue("title")

		if r.Method == http.MethodPost {
			if identifier == "" || title == "" {
				data := ViewData{
					Identifier: test,
					Error:      "Заполните все данные",
				}

				tmpl, _ := template.ParseFiles("index.html")
				tmpl.Execute(w, data)
				return
			}

			textIdentifier := gjson.Get(jsonText.String(), `list.meta.item.#[identifier=="`+identifier+`"].link`)
			textTitle := gjson.Get(jsonText.String(), `list.meta.item.#[title=="`+title+`"].link`)
			if textIdentifier == textTitle {
				data := ViewData{
					Identifier: nil,
					URL:        textIdentifier.Str,
				}

				tmpl, _ := template.ParseFiles("index.html")
				tmpl.Execute(w, data)
				return
			}

			http.ServeFile(w, r, "index.html")
			return
		} else {
			data := ViewData{
				Identifier: test,
			}

			tmpl, _ := template.ParseFiles("index.html")
			tmpl.Execute(w, data)
			return
		}
	})

	if err := http.ListenAndServe(":8081", nil); err != nil {
		panic(err)
	} else {
		log.Println("Сервер запущен..")
	}
}

// tree.branch.0.children.
func searchItem(jsonText string, text string, branch *Branch, res *Response) {
	if strings.Contains(text, branch.Exist) == false {
		return
	} else {
		if branch.Exist != "" {
			countLeaf := gjson.Get(jsonText, text+"leaf.#").Num
			if countLeaf > 0 {
				// Количество листов > 0

				if branch.Exist != "" {
					for i := 0; i < int(countLeaf); i++ {
						res.SDMX = append(res.SDMX, gjson.Get(jsonText, text+"leaf."+strconv.Itoa(i)+".downloadLink.1.#content").Str)
					}
				}
			}
			return
		}
	}

	// Получаем title у одиночной ветки
	getTitle := gjson.Get(jsonText, text+"branch.title.0.#content").Str
	if getTitle != "" {
		// У нас есть title
		// Вызываем функцию с text = tree.branch.children.

		titleBranch := gjson.Get(jsonText, text+"branch.title.0.#content")
		codeBranch := gjson.Get(jsonText, text+"branch.code")
		//fmt.Println(titleBranch)
		//fmt.Println(codeBranch)
		if titleBranch.Str == titleSearch && codeBranch.Str == codeSearch {
			if branch.Exist == "" {
				branch.Exist = text + "branch"
			}

			fmt.Printf("BranchExist: %v \n", branch.Exist)
		}

		searchItem(jsonText, text+"branch.children.", branch, res)
		return
	} else {
		countBranch := gjson.Get(jsonText, text+"branch.#").Num
		if countBranch > 0 {
			// Количество веток > 0

			// tree.branch.0.children.
			for i := 0; i < int(countBranch); i++ {
				titleBranch := gjson.Get(jsonText, text+"branch."+strconv.Itoa(i)+".title.0.#content")
				codeBranch := gjson.Get(jsonText, text+"branch."+strconv.Itoa(i)+".code")
				if titleBranch.Str == titleSearch && codeBranch.Str == codeSearch {
					if branch.Exist == "" {
						branch.Exist = text
					}

					fmt.Printf("BranchExist 2: %v", branch.Exist)
				}

				if strings.Contains(text, branch.Exist) == false {
					break
				}

				searchItem(jsonText, text+"branch."+strconv.Itoa(i)+".children.", branch, res)
			}
		}
	}
}

func DownloadFile(filepath string, url string) error {

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
